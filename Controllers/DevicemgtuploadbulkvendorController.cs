﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtuploadbulkvendorModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtuploadbulkvendorController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtuploadbulkvendorController> _logger;

        public DevicemgtuploadbulkvendorController(ILogger<DevicemgtuploadbulkvendorController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtuploadbulkvendorOutput>> Post(DevicemgtuploadbulkvendorInput req)
        {
            List<DevicemgtuploadbulkvendorOutput> result = new List<DevicemgtuploadbulkvendorOutput>();
            DevicemgtuploadbulkvendorInput objSignInInput = new DevicemgtuploadbulkvendorInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtuploadbulkvendorDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtuploadbulkvendorOutput outputobj = new DevicemgtuploadbulkvendorOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}