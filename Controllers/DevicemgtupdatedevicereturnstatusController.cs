﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtupdatedevicereturnstatusModel;
namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtupdatedevicereturnstatusController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtupdatedevicereturnstatusController> _logger;

        public DevicemgtupdatedevicereturnstatusController(ILogger<DevicemgtupdatedevicereturnstatusController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtupdatedevicereturnstatusOutput>> Post(DevicemgtupdatedevicereturnstatusInput req)
        {
            List<DevicemgtupdatedevicereturnstatusOutput> result = new List<DevicemgtupdatedevicereturnstatusOutput>();
            DevicemgtupdatedevicereturnstatusInput objSignInInput = new DevicemgtupdatedevicereturnstatusInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtupdatedevicereturnstatusDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtupdatedevicereturnstatusOutput outputobj = new DevicemgtupdatedevicereturnstatusOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
