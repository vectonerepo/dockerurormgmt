﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtcreateproductinfoModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtcreateproductinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtcreateproductinfoController> _logger;

        public DevicemgtcreateproductinfoController(ILogger<DevicemgtcreateproductinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtcreateproductinfoOutput>> Post(DevicemgtcreateproductinfoInput req)
        {
            List<DevicemgtcreateproductinfoOutput> result = new List<DevicemgtcreateproductinfoOutput>();
            DevicemgtcreateproductinfoInput objSignInInput = new DevicemgtcreateproductinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtcreateproductinfoDB.CallDB(req, _Config, _logger);
                //if (result.Count > 0)
                //{
                //    try
                //    {
                //        SaveImage();
                //    }
                //    catch (Exception ex)
                //    {

                //        Log.Error("Output Image Saving Error: " + ex.Message);
                //    }

                //}
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtcreateproductinfoOutput outputobj = new DevicemgtcreateproductinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
        //public string SaveImage()
        //{
        //    List<DevicemgtcreateproductinfoOutput> OutputList = new List<DevicemgtcreateproductinfoOutput>();
        //    try
        //    {
              
        //        var httpRequest = HttpContext.Current.Request;
        //        if (httpRequest.Files.Count > 0)
        //        {
        //            Log.Info("UrUploadFiles : Files Count : " + httpRequest.Files.Count);
        //            foreach (string file in httpRequest.Files)
        //            {
        //                var postedFile = httpRequest.Files[file];
        //                DevicemgtcreateproductinfoOutput result1 = new DevicemgtcreateproductinfoOutput();

        //                var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\images\", postedFile.FileName.Replace(" ", string.Empty));
        //                Log.Info("filePath : " + filePath);
        //                postedFile.SaveAs(filePath);
        //                result1.FileName = postedFile.FileName;
        //                result1.FileURL = ConfigurationManager.AppSettings["UploadPath"] + HttpUtility.UrlEncode(postedFile.FileName.Replace(" ", string.Empty));
        //                Log.Info("FileURL : " + result1.FileURL);
        //                try
        //                {
        //                    using (new NetworkConnection(ConfigurationManager.AppSettings["NetworkPath2"], new NetworkCredential(ConfigurationManager.AppSettings["NetworkUsername"], ConfigurationManager.AppSettings["NetworkPassword"], ConfigurationManager.AppSettings["NetworkDomain"])))
        //                    {
        //                        Log.Info("NetworkPath : " + Path.Combine(ConfigurationManager.AppSettings["NetworkPath2"], postedFile.FileName.Replace(" ", string.Empty)));
        //                        postedFile.SaveAs(Path.Combine(ConfigurationManager.AppSettings["NetworkPath2"], postedFile.FileName.Replace(" ", string.Empty)));
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    Log.Error(ex.Message);
        //                }
        //                result1.Code = 0;
        //                result1.Message = "Success";
        //                OutputList.Add(result1);

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        DevicemgtcreateproductinfoOutput outputobj = new DevicemgtcreateproductinfoOutput();
        //        outputobj.Code = -1;
        //        outputobj.Message = ex.Message;
        //        OutputList.Add(outputobj);
        //    }
        //    return "Success";

        //}
    }
    
}
