﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemegtgetproductsellingperformanceModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemegtgetproductsellingperformanceController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemegtgetproductsellingperformanceController> _logger;

        public DevicemegtgetproductsellingperformanceController(ILogger<DevicemegtgetproductsellingperformanceController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;


        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemegtgetproductsellingperformanceOutput>> CreateAsync(DevicemegtgetproductsellingperformanceInput req)
        {
            List<DevicemegtgetproductsellingperformanceOutput> result = new List<DevicemegtgetproductsellingperformanceOutput>();
            DevicemegtgetproductsellingperformanceOutput objSignInInput = new DevicemegtgetproductsellingperformanceOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemegtgetproductsellingperformanceDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemegtgetproductsellingperformanceOutput outputobj = new DevicemegtgetproductsellingperformanceOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }
    }
}