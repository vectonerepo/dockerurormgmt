﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtreturndevicecheckmacserialnumberModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtreturndevicecheckmacserialnumberController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtreturndevicecheckmacserialnumberController> _logger;

        public DevicemgtreturndevicecheckmacserialnumberController(ILogger<DevicemgtreturndevicecheckmacserialnumberController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtreturndevicecheckmacserialnumberOutput>> Post(DevicemgtreturndevicecheckmacserialnumberInput req)
        {
            List<DevicemgtreturndevicecheckmacserialnumberOutput> result = new List<DevicemgtreturndevicecheckmacserialnumberOutput>();
            DevicemgtreturndevicecheckmacserialnumberInput objSignInInput = new DevicemgtreturndevicecheckmacserialnumberInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtreturndevicecheckmacserialnumberDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtreturndevicecheckmacserialnumberOutput outputobj = new DevicemgtreturndevicecheckmacserialnumberOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}