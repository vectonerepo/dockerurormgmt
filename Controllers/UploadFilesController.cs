﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using URORMGTDocker.Models;
using static URORMGTDocker.Models.DevicemgtvalidateuserloginModel;
using System.IO;
using System.Web;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadFilesController : ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<UploadFilesController> _logger;

        public UploadFilesController(ILogger<UploadFilesController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }        
        public class DevicemgtcreateproductinfoOutput : ErrorMessageModel
        {
            public string FileName { get; set; }
            public string FileURL { get; set; }
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtvalidateuserloginOutput>> Post()
        {
            //List<IFormFile> files
            List<DevicemgtvalidateuserloginOutput> result = new List<DevicemgtvalidateuserloginOutput>();
            DevicemgtvalidateuserloginInput objSignInInput = new DevicemgtvalidateuserloginInput();
            List<DevicemgtcreateproductinfoOutput> OutputList = new List<DevicemgtcreateproductinfoOutput>();
            try
            {
                DevicemgtcreateproductinfoOutput result1 = new DevicemgtcreateproductinfoOutput();
                var httpRequest = HttpContext.Request.Form;
                //if (HttpContext.Request.Form.Files[0] != null)
                //{
                //    var file = HttpContext.Request.Form.Files[0];
                //    using (FileStream fs = new FileStream("Your Path", FileMode.CreateNew, FileAccess.Write, FileShare.Write))
                //    {
                //        file.CopyTo(fs);
                //    }
                //}
                if (httpRequest.Files.Count > 0)
                {
                    _logger.LogInformation("UrUploadFiles : Files Count : " + httpRequest.Files.Count);
                    foreach (var file in httpRequest.Files)
                    {
                        var postedFile = Path.GetFileName(file.FileName);


                        var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"logs", "URORDERMGMT", "images", postedFile.Replace(" ", string.Empty));
                        //var filePath = _Config["Uploadfiles:UploadPath"] +@"\images\" + HttpUtility.UrlEncode(postedFile.Replace(" ", string.Empty));
                        _logger.LogInformation("filePath : " + filePath);
                        //postedFile.SaveAs(filePath);
                        result1.FileName = postedFile;
                        result1.FileURL = _Config["Uploadfiles:UploadPath"] + HttpUtility.UrlEncode(postedFile.Replace(" ", string.Empty));
                        _logger.LogInformation("FileURL : " + result1.FileURL);
                        using (FileStream fs = System.IO.File.Create(filePath))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                        //try
                        //{
                        //    using (new NetworkConnection(ConfigurationManager.AppSettings["NetworkPath2"], new NetworkCredential(ConfigurationManager.AppSettings["NetworkUsername"], ConfigurationManager.AppSettings["NetworkPassword"], ConfigurationManager.AppSettings["NetworkDomain"])))
                        //    {
                        //        _logger.LogInformation("NetworkPath : " + Path.Combine(ConfigurationManager.AppSettings["NetworkPath2"], postedFile.FileName.Replace(" ", string.Empty)));
                        //        postedFile.SaveAs(Path.Combine(ConfigurationManager.AppSettings["NetworkPath2"], postedFile.FileName.Replace(" ", string.Empty)));
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    _logger.LogError(ex.Message);
                        //} 
                    }
                    result1.Code = 0;
                    result1.Message = "Success";
                    OutputList.Add(result1);
                    return Ok(result1);

                }
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtvalidateuserloginOutput outputobj = new DevicemgtvalidateuserloginOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }
    }
}