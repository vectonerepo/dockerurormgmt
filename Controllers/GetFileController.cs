﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.IO;
using System.Web;


namespace URORMGTDocker.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<ImagesController> _logger;

        public ImagesController(ILogger<ImagesController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [HttpGet]
        public IActionResult Get(string imagename)
        {
            //var image = System.IO.File.OpenRead("C:\\test\\random_image.jpeg");
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"logs", "URORDERMGMT", "images", imagename);
            //var image = System.IO.File.OpenRead(_Config["Uploadfiles:UploadPath"]+ imagename);
            var image = System.IO.File.OpenRead(filePath);
            return File(image, "image/jpeg");
        }
    }
}
