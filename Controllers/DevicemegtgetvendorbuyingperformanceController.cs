﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemegtgetvendorbuyingperformanceModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemegtgetvendorbuyingperformanceController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemegtgetvendorbuyingperformanceController> _logger;

        public DevicemegtgetvendorbuyingperformanceController(ILogger<DevicemegtgetvendorbuyingperformanceController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemegtgetvendorbuyingperformanceOutput>> Post(DevicemegtgetvendorbuyingperformanceInput req)
        {
            List<DevicemegtgetvendorbuyingperformanceOutput> result = new List<DevicemegtgetvendorbuyingperformanceOutput>();
            DevicemegtgetvendorbuyingperformanceOutput objSignInInput = new DevicemegtgetvendorbuyingperformanceOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemegtgetvendorbuyingperformanceDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemegtgetvendorbuyingperformanceOutput outputobj = new DevicemegtgetvendorbuyingperformanceOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }
    }
}