﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetreturnstatusModel;
using System.Threading;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetreturnstatusController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetreturnstatusController> _logger;

        public DevicemgtgetreturnstatusController(ILogger<DevicemgtgetreturnstatusController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<DevicemgtgetreturnstatusOutput>> Get(CancellationToken cancellationToken)
        {
            List<DevicemgtgetreturnstatusOutput> result = new List<DevicemgtgetreturnstatusOutput>();
            DevicemgtgetreturnstatusOutput objSignInInput = new DevicemgtgetreturnstatusOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetreturnstatusDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetreturnstatusOutput outputobj = new DevicemgtgetreturnstatusOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }
    }
}