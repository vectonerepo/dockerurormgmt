﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetreplacementstatusModel;
using System.Threading;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetreplacementstatusController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetreplacementstatusController> _logger;

        public DevicemgtgetreplacementstatusController(ILogger<DevicemgtgetreplacementstatusController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<DevicemgtgetreplacementstatusOutput>> Get(CancellationToken cancellationToken)
        {
            List<DevicemgtgetreplacementstatusOutput> result = new List<DevicemgtgetreplacementstatusOutput>();
            DevicemgtgetreplacementstatusOutput objSignInInput = new DevicemgtgetreplacementstatusOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetreplacementstatusDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetreplacementstatusOutput outputobj = new DevicemgtgetreplacementstatusOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }

    }
}