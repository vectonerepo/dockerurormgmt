﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicmgtgetassigneddeivcedetailsModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicmgtgetassigneddeivcedetailsController: ControllerBase
    {

        public IConfiguration _Config { get; }
        private readonly ILogger<DevicmgtgetassigneddeivcedetailsController> _logger;

        public DevicmgtgetassigneddeivcedetailsController(ILogger<DevicmgtgetassigneddeivcedetailsController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicmgtgetassigneddeivcedetailsOutput>> Post(DevicmgtgetassigneddeivcedetailsInput req)
        {
            List<DevicmgtgetassigneddeivcedetailsOutput> result = new List<DevicmgtgetassigneddeivcedetailsOutput>();
            DevicmgtgetassigneddeivcedetailsInput objSignInInput = new DevicmgtgetassigneddeivcedetailsInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicmgtgetassigneddeivcedetailsDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicmgtgetassigneddeivcedetailsOutput outputobj = new DevicmgtgetassigneddeivcedetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}