﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetpurchasedordeinfoModel;

namespace UnifiedringOrderMgmt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetpurchasedordeinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetpurchasedordeinfoController> _logger;

        public DevicemgtgetpurchasedordeinfoController(ILogger<DevicemgtgetpurchasedordeinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtgetpurchasedordeinfoOutput>> Post(DevicemgtgetpurchasedordeinfoInput req)
        {
            List<DevicemgtgetpurchasedordeinfoOutput> result = new List<DevicemgtgetpurchasedordeinfoOutput>();
            DevicemgtgetpurchasedordeinfoInput objSignInInput = new DevicemgtgetpurchasedordeinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetpurchasedordeinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetpurchasedordeinfoOutput outputobj = new DevicemgtgetpurchasedordeinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}