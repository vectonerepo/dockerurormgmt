﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtvaidatepromocodeModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtvaidatepromocodeController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtvaidatepromocodeController> _logger;

        public DevicemgtvaidatepromocodeController(ILogger<DevicemgtvaidatepromocodeController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtvaidatepromocodeOutput>> Post(DevicemgtvaidatepromocodeInput req)
        {
            List<DevicemgtvaidatepromocodeOutput> result = new List<DevicemgtvaidatepromocodeOutput>();
            DevicemgtvaidatepromocodeInput objSignInInput = new DevicemgtvaidatepromocodeInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtvaidatepromocodeDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtvaidatepromocodeOutput outputobj = new DevicemgtvaidatepromocodeOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}