﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetproductstatusModel;
using System.Threading;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetproductstatusController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetproductstatusController> _logger;

        public DevicemgtgetproductstatusController(ILogger<DevicemgtgetproductstatusController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<DevicemgtgetproductstatusOutput>> Get()
       // public async Task<ActionResult<DevicemgtgetproductstatusOutput>> Get(CancellationToken cancellationToken)
        {
            List<DevicemgtgetproductstatusOutput> result = new List<DevicemgtgetproductstatusOutput>();
            DevicemgtgetproductstatusOutput objSignInInput = new DevicemgtgetproductstatusOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetproductstatusDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetproductstatusOutput outputobj = new DevicemgtgetproductstatusOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }

    }
}