﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetproductTypeModel;
using System.Threading;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetproductTypeController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetproductTypeController> _logger;

        public DevicemgtgetproductTypeController(ILogger<DevicemgtgetproductTypeController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<DevicemgtgetproductTypeOutput>> Get()
        {
            List<DevicemgtgetproductTypeOutput> result = new List<DevicemgtgetproductTypeOutput>();
            DevicemgtgetproductTypeOutput objSignInInput = new DevicemgtgetproductTypeOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetproductTypeDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
                //return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetproductTypeOutput outputobj = new DevicemgtgetproductTypeOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }
    }
}