﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetreplacementorderinfoModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetreplacementorderinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetreplacementorderinfoController> _logger;

        public DevicemgtgetreplacementorderinfoController(ILogger<DevicemgtgetreplacementorderinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtgetreplacementorderinfoOutput>> Post(DevicemgtgetreplacementorderinfoInput req)
        {
            List<DevicemgtgetreplacementorderinfoOutput> result = new List<DevicemgtgetreplacementorderinfoOutput>();
            DevicemgtgetreplacementorderinfoInput objSignInInput = new DevicemgtgetreplacementorderinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetreplacementorderinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetreplacementorderinfoOutput outputobj = new DevicemgtgetreplacementorderinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}