﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtreplacedevicecheckmacserialnumberModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtreplacedevicecheckmacserialnumberController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtreplacedevicecheckmacserialnumberController> _logger;

        public DevicemgtreplacedevicecheckmacserialnumberController(ILogger<DevicemgtreplacedevicecheckmacserialnumberController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtreplacedevicecheckmacserialnumberOutput>> Post(DevicemgtreplacedevicecheckmacserialnumberInput req)
        {
            List<DevicemgtreplacedevicecheckmacserialnumberOutput> result = new List<DevicemgtreplacedevicecheckmacserialnumberOutput>();
            DevicemgtreplacedevicecheckmacserialnumberInput objSignInInput = new DevicemgtreplacedevicecheckmacserialnumberInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtreplacedevicecheckmacserialnumberDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtreplacedevicecheckmacserialnumberOutput outputobj = new DevicemgtreplacedevicecheckmacserialnumberOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}