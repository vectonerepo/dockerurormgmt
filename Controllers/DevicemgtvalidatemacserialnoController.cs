﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtvalidatemacserialnoModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtvalidatemacserialnoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtvalidatemacserialnoController> _logger;

        public DevicemgtvalidatemacserialnoController(ILogger<DevicemgtvalidatemacserialnoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtvalidatemacserialnoOutput>> Post(DevicemgtvalidatemacserialnoInput req)
        {
            List<DevicemgtvalidatemacserialnoOutput> result = new List<DevicemgtvalidatemacserialnoOutput>();
            DevicemgtvalidatemacserialnoOutput objSignInInput = new DevicemgtvalidatemacserialnoOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtvalidatemacserialnoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtvalidatemacserialnoOutput outputobj = new DevicemgtvalidatemacserialnoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }
    }
}