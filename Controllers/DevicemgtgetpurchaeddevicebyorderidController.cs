﻿using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetpurchaeddevicebyorderidModel;
using Microsoft.Extensions.Configuration;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetpurchaeddevicebyorderidController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetpurchaeddevicebyorderidController> _logger;

        public DevicemgtgetpurchaeddevicebyorderidController(ILogger<DevicemgtgetpurchaeddevicebyorderidController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtgetpurchaeddevicebyorderidOutput>> Post(DevicemgtgetpurchaeddevicebyorderidInput req)
        {
            List<DevicemgtgetpurchaeddevicebyorderidOutput> result = new List<DevicemgtgetpurchaeddevicebyorderidOutput>();
            DevicemgtgetpurchaeddevicebyorderidInput objSignInInput = new DevicemgtgetpurchaeddevicebyorderidInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetpurchaeddevicebyorderidDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

              DevicemgtgetpurchaeddevicebyorderidOutput outputobj = new DevicemgtgetpurchaeddevicebyorderidOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}