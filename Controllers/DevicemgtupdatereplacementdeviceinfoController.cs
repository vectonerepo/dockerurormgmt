﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtupdatereplacementdeviceinfoModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtupdatereplacementdeviceinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtupdatereplacementdeviceinfoController> _logger;

        public DevicemgtupdatereplacementdeviceinfoController(ILogger<DevicemgtupdatereplacementdeviceinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtupdatereplacementdeviceinfoOutput>> Post(DevicemgtupdatereplacementdeviceinfoInput req)
        {
            List<DevicemgtupdatereplacementdeviceinfoOutput> result = new List<DevicemgtupdatereplacementdeviceinfoOutput>();
            DevicemgtupdatereplacementdeviceinfoInput objSignInInput = new DevicemgtupdatereplacementdeviceinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtupdatereplacementdeviceinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtupdatereplacementdeviceinfoOutput outputobj = new DevicemgtupdatereplacementdeviceinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}