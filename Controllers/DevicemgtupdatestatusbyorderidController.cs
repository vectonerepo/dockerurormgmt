﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtupdatestatusbyorderidModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtupdatestatusbyorderidController: ControllerBase
    {

        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtupdatestatusbyorderidController> _logger;

        public DevicemgtupdatestatusbyorderidController(ILogger<DevicemgtupdatestatusbyorderidController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtupdatestatusbyorderidOutput>> Post(DevicemgtupdatestatusbyorderidInput req)
        {
            List<DevicemgtupdatestatusbyorderidOutput> result = new List<DevicemgtupdatestatusbyorderidOutput>();
            DevicemgtupdatestatusbyorderidInput objSignInInput = new DevicemgtupdatestatusbyorderidInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtupdatestatusbyorderidDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtupdatestatusbyorderidOutput outputobj = new DevicemgtupdatestatusbyorderidOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}