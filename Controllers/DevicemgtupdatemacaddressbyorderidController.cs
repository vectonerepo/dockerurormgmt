﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtupdatemacaddressbyorderidModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtupdatemacaddressbyorderidController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtupdatemacaddressbyorderidController> _logger;

        public DevicemgtupdatemacaddressbyorderidController(ILogger<DevicemgtupdatemacaddressbyorderidController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]  
        public async Task<ActionResult<DevicemgtupdatemacaddressbyorderidOutput>> Post(DevicemgtupdatemacaddressbyorderidInput req)
        {
            List<DevicemgtupdatemacaddressbyorderidOutput> result = new List<DevicemgtupdatemacaddressbyorderidOutput>();
            DevicemgtupdatemacaddressbyorderidInput objSignInInput = new DevicemgtupdatemacaddressbyorderidInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtupdatemacaddressbyorderidDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtupdatemacaddressbyorderidOutput outputobj = new DevicemgtupdatemacaddressbyorderidOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}