﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtcreatebrandModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtcreatebrandController: ControllerBase
    {

        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtcreatebrandController> _logger;

        public DevicemgtcreatebrandController(ILogger<DevicemgtcreatebrandController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtcreatebrandOutput>> Post(DevicemgtcreatebrandInput req)
        {
            List<DevicemgtcreatebrandOutput> result = new List<DevicemgtcreatebrandOutput>();
            DevicemgtcreatebrandInput objSignInInput = new DevicemgtcreatebrandInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtcreatebrandDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtcreatebrandOutput outputobj = new DevicemgtcreatebrandOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}