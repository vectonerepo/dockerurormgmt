﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetreplaceinitiatedordersModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetreplaceinitiatedordersController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetreplaceinitiatedordersController> _logger;

        public DevicemgtgetreplaceinitiatedordersController(ILogger<DevicemgtgetreplaceinitiatedordersController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtgetreplaceinitiatedordersOutput>> Post(DevicemgtgetreplaceinitiatedordersInput req)
        {
            List<DevicemgtgetreplaceinitiatedordersOutput> result = new List<DevicemgtgetreplaceinitiatedordersOutput>();
            DevicemgtgetreplaceinitiatedordersInput objSignInInput = new DevicemgtgetreplaceinitiatedordersInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetreplaceinitiatedordersDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetreplaceinitiatedordersOutput outputobj = new DevicemgtgetreplaceinitiatedordersOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}