﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemegtgetsalescustomerbreakdownModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemegtgetsalescustomerbreakdownController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemegtgetsalescustomerbreakdownController> _logger;

        public DevicemegtgetsalescustomerbreakdownController(ILogger<DevicemegtgetsalescustomerbreakdownController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemegtgetsalescustomerbreakdownOutput>> Post(DevicemegtgetsalescustomerbreakdownInput req)
        {
            List<DevicemegtgetsalescustomerbreakdownOutput> result = new List<DevicemegtgetsalescustomerbreakdownOutput>();
            DevicemegtgetsalescustomerbreakdownOutput objSignInInput = new DevicemegtgetsalescustomerbreakdownOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemegtgetsalescustomerbreakdownDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemegtgetsalescustomerbreakdownOutput outputobj = new DevicemegtgetsalescustomerbreakdownOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }
    }
}