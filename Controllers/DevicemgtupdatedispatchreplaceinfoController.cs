﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtupdatedispatchreplaceinfoModel;

namespace UnifiedringOrderMgmt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtupdatedispatchreplaceinfoController:ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtupdatedispatchreplaceinfoController> _logger;

        public DevicemgtupdatedispatchreplaceinfoController(ILogger<DevicemgtupdatedispatchreplaceinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtupdatedispatchreplaceinfoOutput>> Post(DevicemgtupdatedispatchreplaceinfoInput req)
        {
            List<DevicemgtupdatedispatchreplaceinfoOutput> result = new List<DevicemgtupdatedispatchreplaceinfoOutput>();
            DevicemgtupdatedispatchreplaceinfoInput objSignInInput = new DevicemgtupdatedispatchreplaceinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtupdatedispatchreplaceinfoDB.CallDB(req,_Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtupdatedispatchreplaceinfoOutput outputobj = new DevicemgtupdatedispatchreplaceinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(result);
            }
        }
    }
}