﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtcreatevendorModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtcreatevendorController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtcreatevendorController> _logger;

        public DevicemgtcreatevendorController(ILogger<DevicemgtcreatevendorController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtcreatevendorOutput>> Post(DevicemgtcreatevendorInput req)
        {
            List<DevicemgtcreatevendorOutput> result = new List<DevicemgtcreatevendorOutput>();
            DevicemgtcreatevendorInput objSignInInput = new DevicemgtcreatevendorInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtcreatevendorDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtcreatevendorOutput outputobj = new DevicemgtcreatevendorOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}