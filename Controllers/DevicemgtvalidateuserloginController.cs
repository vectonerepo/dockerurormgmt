﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtvalidateuserloginModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtvalidateuserloginController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtvalidateuserloginController> _logger;

        public DevicemgtvalidateuserloginController(ILogger<DevicemgtvalidateuserloginController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtvalidateuserloginOutput>> Post(DevicemgtvalidateuserloginInput req)
        {
            List<DevicemgtvalidateuserloginOutput> result = new List<DevicemgtvalidateuserloginOutput>();
            DevicemgtvalidateuserloginInput objSignInInput = new DevicemgtvalidateuserloginInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtvalidateuserloginDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtvalidateuserloginOutput outputobj = new DevicemgtvalidateuserloginOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}