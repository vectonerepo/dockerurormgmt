﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetproductinfoModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetproductinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetproductinfoController> _logger;

        public DevicemgtgetproductinfoController(ILogger<DevicemgtgetproductinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtgetproductinfoOutput>> Post(DevicemgtgetproductinfoInput req)
        {
            List<DevicemgtgetproductinfoOutput> result = new List<DevicemgtgetproductinfoOutput>();
            DevicemgtgetproductinfoInput objSignInInput = new DevicemgtgetproductinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetproductinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetproductinfoOutput outputobj = new DevicemgtgetproductinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}