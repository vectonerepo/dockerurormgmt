﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;

namespace URORMGTDocker.Controller
{
    [Route("api/[controller]")]
    [ApiController]

    public class RequestTokenController : ControllerBase
    {
        public IConfiguration _Config { get; } 
        private readonly ILogger<RequestTokenController> _logger; 
        public RequestTokenController(ILogger<RequestTokenController> logger, IConfiguration configuration)
        {
            _Config = configuration;
            _logger = logger;
        }
        [HttpGet]
        private bool CheckUser(string username, string password)
        {
           
            string validusername = Base64Encode(_Config["Auth:Username"].Replace("1234", ""));
            string validPassword = Base64Encode(_Config["Auth:Password"].Replace("1234", ""));
            if (username == validusername && password == validPassword)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private static readonly char[] AvailableCharactersNum =
        {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };
        internal static string GenerateIdentifierNum(int length)
        {
            char[] identifier = new char[length];
            byte[] randomData = new byte[length];

            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(randomData);
            }

            for (int idx = 0; idx < identifier.Length; idx++)
            {
                int pos = randomData[idx] % AvailableCharactersNum.Length;
                identifier[idx] = AvailableCharactersNum[pos];
            }

            return new string(identifier);
        }
        public class RequestTokenOutput
        {
            public string AccessToken { get; set; }
            public string Message { get; set; }
            public int Code { get; set; }
        }
        public class RequestTokenInput
        {
            public string SecretId { get; set; }
            public string SecretPassword { get; set; }
        }
        public static class ErrorMessage
        {
            public static Dictionary<int, string> Errors = new Dictionary<int, string>()
                                                {
                                                    {1001,"Success"},
                                                    {1002, "Failure"},
                                                    {1003,"No record found"},
                                                    {1004,"Redis server error"},
                                                    {1005,"Exception,"},
                                                    {1006,"Google Email Verification Failed"},
                                                    {1007,"DB Error"},
                                                    {1008,""},
                                                    {1009,"Email ID not in the correct format"}
                                                };
        }
        [HttpPost]
        public IActionResult Post(RequestTokenInput req)
        {
            _logger.LogInformation("Input : RequestToken:" + JsonConvert.SerializeObject(req));
            
            List<RequestTokenOutput> OutputList = new List<RequestTokenOutput>();
            RequestTokenOutput objRequestTokenOutput = new RequestTokenOutput();
            if (CheckUser(req.SecretId, req.SecretPassword))
            {
                _logger.LogInformation("OutputToken:" + JsonConvert.SerializeObject(objRequestTokenOutput));
                string accesstoken = URORMGTDocker.Auth.JwtAuthManager.GenerateJWTToken(req.SecretId);
                objRequestTokenOutput.AccessToken = accesstoken;
                objRequestTokenOutput.Code = 1001;
                objRequestTokenOutput.Message = ErrorMessage.Errors[1001];
                return Ok(objRequestTokenOutput);                
            }
            
            else
            {
                objRequestTokenOutput.AccessToken = "";
                objRequestTokenOutput.Code = 1002;
                objRequestTokenOutput.Message = ErrorMessage.Errors[1002];
                return Ok(objRequestTokenOutput);
            }
        }
    }
}
