﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtproductpromotionModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtproductpromotionController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtproductpromotionController> _logger;

        public DevicemgtproductpromotionController(ILogger<DevicemgtproductpromotionController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtproductpromotionOutput>> Post(DevicemgtproductpromotionInput req)
        {
            List<DevicemgtproductpromotionOutput> result = new List<DevicemgtproductpromotionOutput>();
            DevicemgtproductpromotionInput objSignInInput = new DevicemgtproductpromotionInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtproductpromotionDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtproductpromotionOutput outputobj = new DevicemgtproductpromotionOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}