﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtcreatepromotionModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtcreatepromotionController: ControllerBase
    {

        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtcreatepromotionController> _logger;

        public DevicemgtcreatepromotionController(ILogger<DevicemgtcreatepromotionController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtcreatepromotionOutput>> Post(DevicemgtcreatepromotionInput req)
        {
            List<DevicemgtcreatepromotionOutput> result = new List<DevicemgtcreatepromotionOutput>();
            DevicemgtcreatepromotionInput objSignInInput = new DevicemgtcreatepromotionInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtcreatepromotionDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtcreatepromotionOutput outputobj = new DevicemgtcreatepromotionOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}