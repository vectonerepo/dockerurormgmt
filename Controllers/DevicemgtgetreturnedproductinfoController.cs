﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtgetreturnedproductinfoModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtgetreturnedproductinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtgetreturnedproductinfoController> _logger;

        public DevicemgtgetreturnedproductinfoController(ILogger<DevicemgtgetreturnedproductinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtgetreturnedproductinfoOutput>> Post(DevicemgtgetreturnedproductinfoInput req)
        {
            List<DevicemgtgetreturnedproductinfoOutput> result = new List<DevicemgtgetreturnedproductinfoOutput>();
            DevicemgtgetreturnedproductinfoInput objSignInInput = new DevicemgtgetreturnedproductinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtgetreturnedproductinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtgetreturnedproductinfoOutput outputobj = new DevicemgtgetreturnedproductinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}