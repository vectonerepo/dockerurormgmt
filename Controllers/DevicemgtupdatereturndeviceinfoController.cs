﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using URORMGTDocker.DB;
using static URORMGTDocker.Models.DevicemgtupdatereturndeviceinfoModel;

namespace URORMGTDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicemgtupdatereturndeviceinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<DevicemgtupdatereturndeviceinfoController> _logger;

        public DevicemgtupdatereturndeviceinfoController(ILogger<DevicemgtupdatereturndeviceinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DevicemgtupdatereturndeviceinfoOutput>> Post(DevicemgtupdatereturndeviceinfoInput req)
        {
            List<DevicemgtupdatereturndeviceinfoOutput> result = new List<DevicemgtupdatereturndeviceinfoOutput>();
            DevicemgtupdatereturndeviceinfoInput objSignInInput = new DevicemgtupdatereturndeviceinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = DevicemgtupdatereturndeviceinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                DevicemgtupdatereturndeviceinfoOutput outputobj = new DevicemgtupdatereturndeviceinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}