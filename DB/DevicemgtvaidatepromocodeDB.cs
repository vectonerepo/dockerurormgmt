﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtvaidatepromocodeModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtvaidatepromocodeDB
    {
        public static List<DevicemgtvaidatepromocodeOutput> CallDB(DevicemgtvaidatepromocodeInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtvaidatepromocodeOutput> OutputList = new List<DevicemgtvaidatepromocodeOutput>();
            _logger.LogInformation("Input : DevicemgtvaidatepromocodeDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_vaidate_promo_code";
                    //var result = conn.Query<dynamic>(
                    object param = new
                    {
                        @promocode = req.promocode
                    };
                    //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtvaidatepromocodeOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtvaidatepromocodeOutput outputobj = new DevicemgtvaidatepromocodeOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtvaidatepromocodeOutput outputobj = new DevicemgtvaidatepromocodeOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}