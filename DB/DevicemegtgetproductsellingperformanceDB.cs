﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using URORMGTDocker.Controllers;
using static URORMGTDocker.Models.DevicemegtgetproductsellingperformanceModel;

namespace URORMGTDocker.DB
{
    public class DevicemegtgetproductsellingperformanceDB
    {
      
        public static List<DevicemegtgetproductsellingperformanceOutput> CallDB(DevicemegtgetproductsellingperformanceInput req, IConfiguration _Config, ILogger _logger)
        {
            _logger.LogInformation("Input : DevicemegtgetproductsellingperformanceDB:" + JsonConvert.SerializeObject(req));
            List<DevicemegtgetproductsellingperformanceOutput> OutputList = new List<DevicemegtgetproductsellingperformanceOutput>();

            //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
            //{
                //conn.Open();
                var sp = "devicemegt_get_product_selling_performance";
                //var result = conn.Query<dynamic>(
                        object param = new
                        {
                            @search_type = req.search_type,
                            @search_operator = req.search_operator,
                            @search_value = req.search_value
                        };
            //commandType: System.Data.CommandType.StoredProcedure);
            IEnumerable<dynamic> result = null;
            DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
            if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new DevicemegtgetproductsellingperformanceOutput()
                    {
                        Product_id = r.Product_id == null ? 0 : r.Product_id,
                        product_name = r.product_name,
                        Porduct_Type = r.Porduct_Type,
                        Sales_qty = r.Sales_qty == null ? 0 : r.Sales_qty,
                        Sales_order_value = r.Sales_order_value == null ? 0.0D : r.Sales_order_value,
                        Sales_Net_Order_value = r.Sales_Net_Order_value == null ? 0.0D : r.Sales_Net_Order_value,
                        buying_currency = r.buying_currency,
                        Selling_currency = r.Selling_currency,
                        exchange_rate = r.exchange_rate == null ? 0.0D : r.exchange_rate,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    DevicemegtgetproductsellingperformanceOutput outputobj = new DevicemegtgetproductsellingperformanceOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            //}
        }
    }
}