﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtproductpromotionModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtproductpromotionDB
    {
        public static List<DevicemgtproductpromotionOutput> CallDB(DevicemgtproductpromotionInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtproductpromotionOutput> OutputList = new List<DevicemgtproductpromotionOutput>();
            _logger.LogInformation("Input : DevicemgtproductpromotionDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_product_promotion";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @promoid = req.promoid,
                                @status = req.status
                            };
                    //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtproductpromotionOutput()
                        {
                            Promoid = r.Promoid == null ? 0 : r.Promoid,
                            product_id = r.product_id == null ? 0 : r.product_id,
                            Product_name = r.Product_name,
                            sku_code = r.sku_code,
                            product_Type_name = r.product_Type_name,
                            product_Type_id = r.product_Type_id == null ? 0 : r.product_Type_id,
                            Start_date = r.Start_date == null ? null : r.Start_date,
                            End_date = r.End_date == null ? null : r.End_date,
                            Promo_code = r.Promo_code,
                            auto_apply = r.auto_apply == null ? 0 : r.auto_apply,
                            status = r.status == null ? 0 : r.status,
                            create_date = r.create_date == null ? null : r.create_date,
                            create_by = r.create_by,
                            last_update = r.last_update == null ? null : r.last_update,
                            last_update_by = r.last_update_by,
                            status_desc = r.status_desc,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtproductpromotionOutput outputobj = new DevicemgtproductpromotionOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtproductpromotionOutput outputobj = new DevicemgtproductpromotionOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}