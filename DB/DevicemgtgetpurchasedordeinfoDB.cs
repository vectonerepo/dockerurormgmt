﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtgetpurchasedordeinfoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetpurchasedordeinfoDB
    {
        public static List<DevicemgtgetpurchasedordeinfoOutput> CallDB(DevicemgtgetpurchasedordeinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetpurchasedordeinfoOutput> OutputList = new List<DevicemgtgetpurchasedordeinfoOutput>();
            _logger.LogInformation("Input : DevicemgtgetpurchasedordeinfoDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_get_purchased_orde_info";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @Search_type = req.Search_type
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtgetpurchasedordeinfoOutput()
                        {
                            order_no = r.order_no, //== null ? 0 : r.order_no,
                            company_name = r.company_name, //== null ? 0 : r.company_name,
                            contact_phone = r.contact_phone,
                            email = r.email,
                            shipping_address = r.shipping_address,
                            product_name = r.product_name,
                            sku_code = r.sku_code,
                            phone_model = r.phone_model,
                            tot_phone_charge = r.tot_phone_charge == null ? 0.0D : r.tot_phone_charge,
                            quantity = r.quantity == null ? 0 : r.quantity,
                            createdate = r.createdate == null ? null : r.createdate,
                            shipment_date = r.shipment_date == null ? null : r.shipment_date,
                            dispatch_date = r.dispatch_date == null ? null : r.dispatch_date,
                            dev_Status = r.dev_Status,
                            delivery_date =  r.delivery_date == null ? null : r.delivery_date,
                            customer_id = r.customer_id == null ? 0 : r.customer_id,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtgetpurchasedordeinfoOutput outputobj = new DevicemgtgetpurchasedordeinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtgetpurchasedordeinfoOutput outputobj = new DevicemgtgetpurchasedordeinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}