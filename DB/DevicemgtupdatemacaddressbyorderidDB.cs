﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtupdatemacaddressbyorderidModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtupdatemacaddressbyorderidDB
    {
        public static List<DevicemgtupdatemacaddressbyorderidOutput> CallDB(DevicemgtupdatemacaddressbyorderidInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtupdatemacaddressbyorderidOutput> OutputList = new List<DevicemgtupdatemacaddressbyorderidOutput>();
            _logger.LogInformation("Input : DevicemgtupdatemacaddressbyorderidDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_update_mac_address_by_orderid";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @order_id = req.order_id,
                                @mac_address = req.mac_address,
                                @serial_number = req.serial_number
                            };
                            //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtupdatemacaddressbyorderidOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtupdatemacaddressbyorderidOutput outputobj = new DevicemgtupdatemacaddressbyorderidOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtupdatemacaddressbyorderidOutput outputobj = new DevicemgtupdatemacaddressbyorderidOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}