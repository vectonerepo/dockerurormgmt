﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtupdatereturndeviceinfoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtupdatereturndeviceinfoDB
    {
        public static List<DevicemgtupdatereturndeviceinfoOutput> CallDB(DevicemgtupdatereturndeviceinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtupdatereturndeviceinfoOutput> OutputList = new List<DevicemgtupdatereturndeviceinfoOutput>();
            _logger.LogInformation("Input : DevicemgtupdatereturndeviceinfoDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_update_return_device_info";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @return_id = req.return_id,
                                @sku_code = req.sku_code,
                                @comments = req.comments,
                                @return_status = req.return_status,
                                @return_reason = req.return_reason,
                                @Type = req.Type,
                                @return_reason_Type = req.return_reason_Type
                            };
                    //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtupdatereturndeviceinfoOutput()
                        {                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtupdatereturndeviceinfoOutput outputobj = new DevicemgtupdatereturndeviceinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtupdatereturndeviceinfoOutput outputobj = new DevicemgtupdatereturndeviceinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}