﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtupdatereplacementdeviceinfoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtupdatereplacementdeviceinfoDB
    {
        public static List<DevicemgtupdatereplacementdeviceinfoOutput> CallDB(DevicemgtupdatereplacementdeviceinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtupdatereplacementdeviceinfoOutput> OutputList = new List<DevicemgtupdatereplacementdeviceinfoOutput>();
            _logger.LogInformation("Input : DevicemgtupdatereplacementdeviceinfoDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_update_replacement_device_info";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @replacementid = req.replacementid,
                                @sku_code = req.sku_code,
                                @comments = req.comments,
                                @return_status = req.return_status,
                                @return_reason = req.return_reason,
                                @Type = req.Type,
                                @reason_type = req.reason_type
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtupdatereplacementdeviceinfoOutput()
                        {                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtupdatereplacementdeviceinfoOutput outputobj = new DevicemgtupdatereplacementdeviceinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtupdatereplacementdeviceinfoOutput outputobj = new DevicemgtupdatereplacementdeviceinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}