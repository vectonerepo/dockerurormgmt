﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using URORMGTDocker.Controllers;
using static URORMGTDocker.Models.DevicemegtgetsalescustomerbreakdownModel;

namespace URORMGTDocker.DB
{
    public class DevicemegtgetsalescustomerbreakdownDB
    {        
        public static List<DevicemegtgetsalescustomerbreakdownOutput> CallDB(DevicemegtgetsalescustomerbreakdownInput req, IConfiguration _Config, ILogger _logger)
        {
            _logger.LogInformation("Input : DevicemegtgetsalescustomerbreakdownDB:" + JsonConvert.SerializeObject(req));
            List<DevicemegtgetsalescustomerbreakdownOutput> OutputList = new List<DevicemegtgetsalescustomerbreakdownOutput>();

            //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
            //{
                //conn.Open();
                var sp = "devicemegt_get_sales_customer_break_down";
                //var result = conn.Query<dynamic>(
                        object param = new
                        {
                            @search_type = req.search_type,
                            @search_operator = req.search_operator,
                            @search_value = req.search_value
                        };
            //commandType: System.Data.CommandType.StoredProcedure);
            IEnumerable<dynamic> result = null;
            DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
            if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new DevicemegtgetsalescustomerbreakdownOutput()
                    {
                        customer_id = r.customer_id == null ? 0 : r.customer_id,
                        cust_address = r.cust_address,
                        Vendor_name = r.Vendor_name,
                        Order_placed = r.Order_placed == null ? 0 : r.Order_placed,
                        Success_order = r.Success_order == null ? 0 : r.Success_order,
                        Net_sale = r.Net_sale == null ? 0.0D : r.Net_sale,
                        Gross_Profit = r.Gross_Profit == null ? 0.0D : r.Gross_Profit,
                        Shipment_cost = r.Shipment_cost == null ? 0.0D : r.Shipment_cost,
                        tax = r.tax == null ? 0.0D : r.tax,
                        discount = r.discount == null ? 0.0D : r.discount,
                        Goods_cost = r.Goods_cost == null ? 0.0D : r.Goods_cost,
                        buying_currency = r.buying_currency,
                        Selling_currency = r.Selling_currency,
                        exchange_rate = r.exchange_rate == null ? 0.0D : r.exchange_rate,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    DevicemegtgetsalescustomerbreakdownOutput outputobj = new DevicemegtgetsalescustomerbreakdownOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            //}
        }
    }
}