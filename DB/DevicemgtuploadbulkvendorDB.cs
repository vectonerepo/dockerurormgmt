﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtuploadbulkvendorModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtuploadbulkvendorDB
    {
        public static List<DevicemgtuploadbulkvendorOutput> CallDB(DevicemgtuploadbulkvendorInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtuploadbulkvendorOutput> OutputList = new List<DevicemgtuploadbulkvendorOutput>();
            _logger.LogInformation("Input : DevicemgtuploadbulkvendorDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_upload_bulk_vendor";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @vendor_xml = req.vendor_xml,
                                @create_by = req.create_by
                            };
                ////commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtuploadbulkvendorOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtuploadbulkvendorOutput outputobj = new DevicemgtuploadbulkvendorOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtuploadbulkvendorOutput outputobj = new DevicemgtuploadbulkvendorOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}