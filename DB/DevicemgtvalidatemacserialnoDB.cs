﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using URORMGTDocker.Controllers;
using static URORMGTDocker.Models.DevicemgtvalidatemacserialnoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtvalidatemacserialnoDB
    {
        public static List<DevicemgtvalidatemacserialnoOutput> CallDB(DevicemgtvalidatemacserialnoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtvalidatemacserialnoOutput> OutputList = new List<DevicemgtvalidatemacserialnoOutput>();

            //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
            //{
                //conn.Open();
                var sp = "devicemgt_validate_mac_serial_no";
                //var result = conn.Query<dynamic>(
                        object param = new
                        {
                            @mac_address = req.mac_address,
                            @serial_no = req.serial_no
                        };
                //commandType: System.Data.CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new DevicemgtvalidatemacserialnoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    DevicemgtvalidatemacserialnoOutput outputobj = new DevicemgtvalidatemacserialnoOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            //}
        }
    }
}