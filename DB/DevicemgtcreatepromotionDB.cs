﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtcreatepromotionModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtcreatepromotionDB
    {
        public static List<DevicemgtcreatepromotionOutput> CallDB(DevicemgtcreatepromotionInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtcreatepromotionOutput> OutputList = new List<DevicemgtcreatepromotionOutput>();
            _logger.LogInformation("Input : DevicemgtcreatepromotionDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_create_promotion";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @product_id = req.product_id,
                                @Min_pur_discount = req.Min_pur_discount,
                                @product_Type = req.product_Type,
                                @Start_date = req.Start_date,
                                @end_date = req.end_date,
                                @Promo_code = req.Promo_code,
                                @auto_apply = req.auto_apply,
                                @status = req.status,
                                @calledby = req.calledby,
                                @promoid = req.promoid,
                                @process_Type = req.process_Type
                            };
                    //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtcreatepromotionOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtcreatepromotionOutput outputobj = new DevicemgtcreatepromotionOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtcreatepromotionOutput outputobj = new DevicemgtcreatepromotionOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}