﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtupdatedispatchreplaceinfoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtupdatedispatchreplaceinfoDB
    {
        public static List<DevicemgtupdatedispatchreplaceinfoOutput> CallDB(DevicemgtupdatedispatchreplaceinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtupdatedispatchreplaceinfoOutput> OutputList = new List<DevicemgtupdatedispatchreplaceinfoOutput>();
            _logger.LogInformation("Input : DevicemgtupdatedispatchreplaceinfoDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_update_dispatch_replace_info";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @replace_id = req.replace_id,
                                @mac_address = req.mac_address,
                                @serial_number = req.serial_number,
                                @status = req.status,
                                @Type = req.Type,
                                @shipping_date = req.shipping_date,
                                @track_number = req.track_number,
                                @tracking_url = req.tracking_url
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtupdatedispatchreplaceinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtupdatedispatchreplaceinfoOutput outputobj = new DevicemgtupdatedispatchreplaceinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtupdatedispatchreplaceinfoOutput outputobj = new DevicemgtupdatedispatchreplaceinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}