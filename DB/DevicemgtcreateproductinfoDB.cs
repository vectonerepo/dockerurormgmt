﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtcreateproductinfoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtcreateproductinfoDB
    {
        public static List<DevicemgtcreateproductinfoOutput> CallDB(DevicemgtcreateproductinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtcreateproductinfoOutput> OutputList = new List<DevicemgtcreateproductinfoOutput>();
            _logger.LogInformation("Input : DevicemgtcreateproductinfoDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_create_product_info";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @Product_name = req.Product_name,
                                @product_desc = req.product_desc,
                                @sku_code = req.sku_code,
                                @Fk_product_type_id = req.Fk_product_type_id,
                                @Fk_brand_Id = req.Fk_brand_Id,
                                @buying_currency = req.buying_currency,
                                @buying_price = req.buying_price,
                                @selling_currency = req.selling_currency,
                                @selling_price = req.selling_price,
                                @weight = req.weight,
                                @stock = req.stock,
                                @status = req.status,
                                @Fk_Vendor_ID = req.Fk_Vendor_ID,
                                @img_path1 = req.img_path1,
                                @img_path2 = req.img_path2,
                                @img_path3 = req.img_path3,
                                @img_path4 = req.img_path4,
                                @img_path5 = req.img_path5,
                                @create_by = req.create_by,
                                @product_id = req.product_id,
                                @process_Type = req.process_Type
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtcreateproductinfoOutput()
                        {                           
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtcreateproductinfoOutput outputobj = new DevicemgtcreateproductinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtcreateproductinfoOutput outputobj = new DevicemgtcreateproductinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}