﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtcreatebrandModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtcreatebrandDB
    {        
        public static List<DevicemgtcreatebrandOutput> CallDB(DevicemgtcreatebrandInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtcreatebrandOutput> OutputList = new List<DevicemgtcreatebrandOutput>();
            _logger.LogInformation("Input : DevicemgtcreatebrandDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_create_brand";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @brand_name = req.brand_name,
                                @status = req.status,
                                @brandid = req.brandid,
                                @create_by = req.create_by,
                                @processtype = req.processtype
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtcreatebrandOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtcreatebrandOutput outputobj = new DevicemgtcreatebrandOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtcreatebrandOutput outputobj = new DevicemgtcreatebrandOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}