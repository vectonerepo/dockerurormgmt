﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtgetbrandModel;
namespace URORMGTDocker.DB
{
    public class DevicemgtgetbrandDB
    {
        public static List<DevicemgtgetbrandOutput> CallDB(DevicemgtgetbrandInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetbrandOutput> OutputList = new List<DevicemgtgetbrandOutput>();
            _logger.LogInformation("Input : DevicemgtgetbrandDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_get_brand";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @brand_id = req.brand_id
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtgetbrandOutput()
                        {
                            brand_id = r.brand_id == null ? 0 : r.brand_id,
                            brand_name = r.brand_name,
                            status = r.status == null ? 0 : r.status,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtgetbrandOutput outputobj = new DevicemgtgetbrandOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtgetbrandOutput outputobj = new DevicemgtgetbrandOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}