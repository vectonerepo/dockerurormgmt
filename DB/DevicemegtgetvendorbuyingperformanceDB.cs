﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using URORMGTDocker.Controllers;
using static URORMGTDocker.Models.DevicemegtgetvendorbuyingperformanceModel;

namespace URORMGTDocker.DB
{
    public class DevicemegtgetvendorbuyingperformanceDB
    {
        public static List<DevicemegtgetvendorbuyingperformanceOutput> CallDB(DevicemegtgetvendorbuyingperformanceInput req, IConfiguration _Config, ILogger _logger)
        {
            _logger.LogInformation("Input : DevicemegtgetvendorbuyingperformanceDB:" + JsonConvert.SerializeObject(req));
            List<DevicemegtgetvendorbuyingperformanceOutput> OutputList = new List<DevicemegtgetvendorbuyingperformanceOutput>();

            //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
            //{
                //conn.Open();
                var sp = "devicemegt_get_vendor_buying_performance";
                //var result = conn.Query<dynamic>(
                        object param = new
                        {
                            @search_type = req.search_type,
                            @search_operator = req.search_operator,
                            @search_value = req.search_value
                        };
            //commandType: System.Data.CommandType.StoredProcedure);
            IEnumerable<dynamic> result = null;
            DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
            if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new DevicemegtgetvendorbuyingperformanceOutput()
                    {
                        Vendor_name = r.Vendor_name, //== null ? 0 : r.Vendor_name,
                        Product_name = r.Product_name,
                        brand_name = r.brand_name,
                        name = r.name ,//== null ? 0 : r.name,
                        buying_price = r.buying_price == null ? 0.0D : r.buying_price,
                        buying_currency = r.buying_currency,
                        qty = r.qty == null ? 0 : Convert.ToInt32(r.qty),
                        Selling_currency = r.Selling_currency,
                        exchange_rate = r.exchange_rate == null ? 0.0D : r.exchange_rate,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    DevicemegtgetvendorbuyingperformanceOutput outputobj = new DevicemegtgetvendorbuyingperformanceOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            //}
        }
    }
}