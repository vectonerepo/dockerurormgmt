﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtgetpurchaeddevicebyorderidModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetpurchaeddevicebyorderidDB
    {
        public static List<DevicemgtgetpurchaeddevicebyorderidOutput> CallDB(DevicemgtgetpurchaeddevicebyorderidInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetpurchaeddevicebyorderidOutput> OutputList = new List<DevicemgtgetpurchaeddevicebyorderidOutput>();
            _logger.LogInformation("Input : DevicemgtgetpurchaeddevicebyorderidDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_get_purchaed_device_by_orderid";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @order_id = req.order_id
                            };
                    //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtgetpurchaeddevicebyorderidOutput()
                        {
                            order_no = r.order_no,
                            Product_name = r.Product_name,
                            sku_code = r.sku_code,
                            customer_id = r.customer_id == null ? 0 : r.customer_id,
                            email = r.email,
                            mobileno = r.mobileno,
                            address1 = r.address1,
                            address2 = r.address2,
                            city = r.city,
                            postcode = r.postcode,
                            country = r.country,
                            dev_Status = r.dev_Status,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtgetpurchaeddevicebyorderidOutput outputobj = new DevicemgtgetpurchaeddevicebyorderidOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtgetpurchaeddevicebyorderidOutput outputobj = new DevicemgtgetpurchaeddevicebyorderidOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}