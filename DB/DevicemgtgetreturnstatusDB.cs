﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using URORMGTDocker.Controllers;
using static URORMGTDocker.Models.DevicemgtgetreturnstatusModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetreturnstatusDB
    {
        public static List<DevicemgtgetreturnstatusOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetreturnstatusOutput> OutputList = new List<DevicemgtgetreturnstatusOutput>();

            //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
            //{
                //conn.Open();
                var sp = "devicemgt_get_return_status";
                //var result = conn.Query<dynamic>(
                        object param = new
                        {
                        };
            //commandType: System.Data.CommandType.StoredProcedure);
            IEnumerable<dynamic> result = null;
            DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
            if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new DevicemgtgetreturnstatusOutput()
                    {
                        status_id = r.status_id == null ? 0 : r.status_id,
                        status_desc = r.status_desc,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    DevicemgtgetreturnstatusOutput outputobj = new DevicemgtgetreturnstatusOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            //}
        }
    }
}