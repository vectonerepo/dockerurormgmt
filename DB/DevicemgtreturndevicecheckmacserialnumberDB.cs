﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtreturndevicecheckmacserialnumberModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtreturndevicecheckmacserialnumberDB
    {
        public static List<DevicemgtreturndevicecheckmacserialnumberOutput> CallDB(DevicemgtreturndevicecheckmacserialnumberInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtreturndevicecheckmacserialnumberOutput> OutputList = new List<DevicemgtreturndevicecheckmacserialnumberOutput>();
            _logger.LogInformation("Input : DevicemgtreturndevicecheckmacserialnumberDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_return_device_check_mac_serial_number";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @return_id = req.return_id,
                                @sku_code = req.sku_code,
                                @mac_address = req.mac_address,
                                @serial_no = req.serial_no
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtreturndevicecheckmacserialnumberOutput()
                        {                           
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtreturndevicecheckmacserialnumberOutput outputobj = new DevicemgtreturndevicecheckmacserialnumberOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtreturndevicecheckmacserialnumberOutput outputobj = new DevicemgtreturndevicecheckmacserialnumberOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}