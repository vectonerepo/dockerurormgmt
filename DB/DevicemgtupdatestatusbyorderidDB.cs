﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtupdatestatusbyorderidModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtupdatestatusbyorderidDB
    {
        public static List<DevicemgtupdatestatusbyorderidOutput> CallDB(DevicemgtupdatestatusbyorderidInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtupdatestatusbyorderidOutput> OutputList = new List<DevicemgtupdatestatusbyorderidOutput>();
            _logger.LogInformation("Input : DevicemgtupdatestatusbyorderidDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_update_status_by_orderid";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @order_id = req.order_id,
                                @status = req.status,
                                @tracking_url = req.tracking_url,
                                @tracking_no = req.tracking_no,
                                @shipment_date = req.shipment_date
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtupdatestatusbyorderidOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtupdatestatusbyorderidOutput outputobj = new DevicemgtupdatestatusbyorderidOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtupdatestatusbyorderidOutput outputobj = new DevicemgtupdatestatusbyorderidOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}