﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtgetreplaceinitiatedordersModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetreplaceinitiatedordersDB
    {
        public static List<DevicemgtgetreplaceinitiatedordersOutput> CallDB(DevicemgtgetreplaceinitiatedordersInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetreplaceinitiatedordersOutput> OutputList = new List<DevicemgtgetreplaceinitiatedordersOutput>();
            _logger.LogInformation("Input : DevicemgtgetreplaceinitiatedordersDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_get_replace_initiated_orders";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @status = req.status,
                                @replace_id = req.replace_id
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtgetreplaceinitiatedordersOutput()
                        {
                            replace_id = r.replace_id == null ? 0 : r.replace_id,
                            order_id = r.order_id == null ? 0 : r.order_id,
                            prod_code = r.prod_code,
                            prod_name = r.prod_name,
                            prod_model = r.prod_model,
                            rpl_date = r.rpl_date == null ? null : r.rpl_date,
                            rpl_quantity = r.rpl_quantity == null ? 0 : r.rpl_quantity,
                            rpl_reason = r.rpl_reason,
                            rpl_status = r.rpl_status == null ? 0 : r.rpl_status,
                            replace_date = r.replace_date == null ? null : r.replace_date,
                            last_update = r.last_update == null ? null : r.last_update,
                            delivery_date = r.delivery_date == null ? null : r.delivery_date,
                            shipping_date = r.shipping_date == null ? null : r.shipping_date,
                            shipping_address = r.shipping_address,
                            email_id = r.email_id,
                            Mobileno = r.Mobileno,
                            customer_id = r.customer_id == null ? 0 : r.customer_id,
                            rpl_status_desc = r.rpl_status_desc,
                            sku_code = r.sku_code,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtgetreplaceinitiatedordersOutput outputobj = new DevicemgtgetreplaceinitiatedordersOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtgetreplaceinitiatedordersOutput outputobj = new DevicemgtgetreplaceinitiatedordersOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}