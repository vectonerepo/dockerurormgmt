﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtgetproductinfoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetproductinfoDB
    {
        public static List<DevicemgtgetproductinfoOutput> CallDB(DevicemgtgetproductinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetproductinfoOutput> OutputList = new List<DevicemgtgetproductinfoOutput>();
            _logger.LogInformation("Input : DevicemgtgetproductinfoDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_get_product_info";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @product_id = req.product_id,
                                @vendor_id = req.vendor_id,
                                @brand_id = req.brand_id,
                                @stock_qty_filter = req.stock_qty_filter,
                                @status = req.status
                            };
                    //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtgetproductinfoOutput()
                        {
                            Product_Id = r.Product_Id == null ? 0 : r.Product_Id,
                            Product_name = r.Product_name,
                            product_desc = r.product_desc,
                            sku_code = r.sku_code,
                            Fk_product_type_id = r.Fk_product_type_id == null ? 0 : r.Fk_product_type_id,
                            Fk_brand_Id = r.Fk_brand_Id == null ? 0 : r.Fk_brand_Id,
                            buying_currency = r.buying_currency,
                            buying_price = r.buying_price == null ? 0.0F : r.buying_price,
                            selling_currency = r.selling_currency,
                            selling_price = r.selling_price == null ? 0.0F : r.selling_price,
                            weight = r.weight == null ? 0.0F : r.weight,
                            stock = r.stock == null ? 0 : r.stock,
                            status = r.status == null ? 0 : r.status,
                            Fk_Vendor_ID = r.Fk_Vendor_ID == null ? 0 : r.Fk_Vendor_ID,
                            img_path1 = r.img_path1,
                            img_path2 = r.img_path2,
                            img_path3 = r.img_path3,
                            img_path4 = r.img_path4,
                            img_path5 = r.img_path5,
                            product_type_desc = r.product_type_desc,
                            brand_desc = r.brand_desc,
                            Vendor_name = r.Vendor_name,
                            status_description = r.status_description,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg,
                            create_date = r.create_date == null ? null : r.create_date,
                            last_update = r.last_update == null ? null : r.last_update
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtgetproductinfoOutput outputobj = new DevicemgtgetproductinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtgetproductinfoOutput outputobj = new DevicemgtgetproductinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}