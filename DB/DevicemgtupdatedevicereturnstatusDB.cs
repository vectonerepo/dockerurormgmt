﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtupdatedevicereturnstatusModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtupdatedevicereturnstatusDB
    {
        public static List<DevicemgtupdatedevicereturnstatusOutput> CallDB(DevicemgtupdatedevicereturnstatusInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtupdatedevicereturnstatusOutput> OutputList = new List<DevicemgtupdatedevicereturnstatusOutput>();
            _logger.LogInformation("Input : DevicemgtupdatedevicereturnstatusDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicemgt_update_device_return_status";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @returnid = req.returnid,
                                @status = req.status
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtupdatedevicereturnstatusOutput()
                        {                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtupdatedevicereturnstatusOutput outputobj = new DevicemgtupdatedevicereturnstatusOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtupdatedevicereturnstatusOutput outputobj = new DevicemgtupdatedevicereturnstatusOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}