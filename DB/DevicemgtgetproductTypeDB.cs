﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using URORMGTDocker.Controllers;
using static URORMGTDocker.Models.DevicemgtgetproductTypeModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetproductTypeDB
    {
        public static List<DevicemgtgetproductTypeOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetproductTypeOutput> OutputList = new List<DevicemgtgetproductTypeOutput>();

            //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
            //{
                //conn.Open();
                var sp = "device_mgt_get_product_Type";
                //var result = conn.Query<dynamic>(
                        object param = new
                        {
                        };
            //commandType: System.Data.CommandType.StoredProcedure);
            IEnumerable<dynamic> result = null;
            DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
            if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new DevicemgtgetproductTypeOutput()
                    {

                        product_Type = r.product_Type == null ? 0 : r.product_Type,
                        name = r.name,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    DevicemgtgetproductTypeOutput outputobj = new DevicemgtgetproductTypeOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            //}
        }

    }
}