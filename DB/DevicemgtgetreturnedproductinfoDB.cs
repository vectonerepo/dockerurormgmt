﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtgetreturnedproductinfoModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetreturnedproductinfoDB
    {
        public static List<DevicemgtgetreturnedproductinfoOutput> CallDB(DevicemgtgetreturnedproductinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetreturnedproductinfoOutput> OutputList = new List<DevicemgtgetreturnedproductinfoOutput>();
            _logger.LogInformation("Input : DevicemgtgetreturnedproductinfoDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_get_returned_product_info";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @Search_type = req.Search_type,
                                @retrun_id = req.retrun_id
                            };
                    //commandType: CommandType.StoredProcedure);
                    IEnumerable<dynamic> result = null;
                    DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtgetreturnedproductinfoOutput()
                        {
                            order_no = r.order_no, //== null ? 0 : r.order_no,
                            company_name = r.company_name,
                            contact_phone = r.contact_phone,
                            email = r.email,
                            shipping_address = r.shipping_address,
                            product_name = r.product_name,
                            sku_code = r.sku_code,
                            phone_model = r.phone_model,
                            rtn_charge = r.rtn_charge == null ? 0.0F : r.rtn_charge,
                            quantity = r.quantity == null ? 0 : r.quantity,
                            return_Date = r.return_Date == null ? null : r.return_Date,
                            shipment_date = r.shipment_date == null ? null : r.shipment_date,
                            dispatch_date = r.dispatch_date == null ? null : r.dispatch_date,
                            rtn_reason = r.rtn_reason,
                            status_id = r.status_id == null ? 0 : r.status_id,
                            status_desc = r.status_desc,
                            return_id = r.return_id == null ? 0 : r.return_id,
                            payment_mode = r.payment_mode,
                            customer_id = r.customer_id == null ? 0 : r.customer_id,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtgetreturnedproductinfoOutput outputobj = new DevicemgtgetreturnedproductinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtgetreturnedproductinfoOutput outputobj = new DevicemgtgetreturnedproductinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}