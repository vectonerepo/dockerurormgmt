﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtvalidateuserloginModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtvalidateuserloginDB
    {
        public static List<DevicemgtvalidateuserloginOutput> CallDB(DevicemgtvalidateuserloginInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtvalidateuserloginOutput> OutputList = new List<DevicemgtvalidateuserloginOutput>();
            _logger.LogInformation("Input : DevicemgtvalidateuserloginDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_validate_user_login";
                //var result = conn.Query<dynamic>(
                object param = new
                {
                    @username = req.username,
                    @password = req.password,
                    @loginip = req.loginip
                };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");

                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtvalidateuserloginOutput()
                        {
                            roleid = r.roleid == null ? 0 : r.roleid,
                            userid = r.userid == null ? 0 : r.userid,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtvalidateuserloginOutput outputobj = new DevicemgtvalidateuserloginOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtvalidateuserloginOutput outputobj = new DevicemgtvalidateuserloginOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}