﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicemgtgetvendorModel;

namespace URORMGTDocker.DB
{
    public class DevicemgtgetvendorDB
    {
        public static List<DevicemgtgetvendorOutput> CallDB(DevicemgtgetvendorInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicemgtgetvendorOutput> OutputList = new List<DevicemgtgetvendorOutput>();
            _logger.LogInformation("Input : DevicemgtgetvendorDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "device_mgt_get_vendor";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @vendor_id = req.vendor_id,
                                @status = req.status,
                            };
                //commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicemgtgetvendorOutput()
                        {
                            Vendor_ID = r.Vendor_ID == null ? 0 : r.Vendor_ID,
                            Vendor_name = r.Vendor_name,
                            status = r.status == null ? 0 : r.status,
                            email_id = r.email_id,
                            phoneno = r.phoneno,
                            location = r.location,
                            last_update = r.last_update == null ? null : r.last_update,
                            status_desc = r.status_desc,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicemgtgetvendorOutput outputobj = new DevicemgtgetvendorOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicemgtgetvendorOutput outputobj = new DevicemgtgetvendorOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}