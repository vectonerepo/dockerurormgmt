﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URORMGTDocker.Models.DevicmgtgetassigneddeivcedetailsModel;

namespace URORMGTDocker.DB
{
    public class DevicmgtgetassigneddeivcedetailsDB
    {
        public static List<DevicmgtgetassigneddeivcedetailsOutput> CallDB(DevicmgtgetassigneddeivcedetailsInput req, IConfiguration _Config, ILogger _logger)
        {
            List<DevicmgtgetassigneddeivcedetailsOutput> OutputList = new List<DevicmgtgetassigneddeivcedetailsOutput>();
            _logger.LogInformation("Input : DevicmgtgetassigneddeivcedetailsDB:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:DEVICEMGMT"]))
                //{
                    //conn.Open();
                    var sp = "devicmgt_get_assigned_deivce_details";
                    //var result = conn.Query<dynamic>(
                            object param = new
                            {
                                @order_id = req.order_id,
                                @type = req.type,
                                @Search_status = req.Search_status
                            };
                ////commandType: CommandType.StoredProcedure);
                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:DEVICEMGMT");
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new DevicmgtgetassigneddeivcedetailsOutput()
                        {
                            order_no = r.order_no,
                            Product_name = r.Product_name,
                            sku_code = r.sku_code,
                            customer_id = r.customer_id == null ? 0 : r.customer_id,
                            email = r.email,
                            mobileno = r.mobileno,
                            address1 = r.address1,
                            address2 = r.address2,
                            city = r.city,
                            postcode = r.postcode,
                            country = r.country,
                            dev_Status = r.dev_Status,
                            quantity = r.quantity == null ? 0 : r.quantity,
                            tot_charge = r.tot_charge == null ? 0.0F : r.tot_charge,
                            delivery_date = r.delivery_date == null ? null : r.delivery_date,
                            shipment_date = r.shipment_date == null ? null : r.shipment_date,
                            serial_number = r.serial_number,
                            mac_address = r.mac_address,
                            createdate = r.createdate == null ? null: r.createdate,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        DevicmgtgetassigneddeivcedetailsOutput outputobj = new DevicmgtgetassigneddeivcedetailsOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                DevicmgtgetassigneddeivcedetailsOutput outputobj = new DevicmgtgetassigneddeivcedetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}