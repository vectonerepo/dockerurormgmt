﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URORMGTDocker.Models.urcrmportalgetticketdeptModel;

namespace URORMGTDocker.DB
{
    public class urcrmportalgetticketdeptDB
    {
        
        public static List<urcrmportalgetticketdeptOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetticketdeptOutput> OutputList = new List<urcrmportalgetticketdeptOutput>();

            using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
                conn.Open();
                var sp = "ur_crm_portal_get_ticket_dept";
                var result = conn.Query<dynamic>(
                        sp, new
                        {
                        },
                        commandType: System.Data.CommandType.StoredProcedure);
                if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new urcrmportalgetticketdeptOutput()
                    {
                        dept_id = r.dept_id == null ? 0 : r.dept_id,
                        dept_name = r.dept_name,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    urcrmportalgetticketdeptOutput outputobj = new urcrmportalgetticketdeptOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }

    }
}