﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtuploadbulkvendorModel
    {
        public class DevicemgtuploadbulkvendorInput
        {
            public string vendor_xml { get; set; }
            public string create_by { get; set; }
        }
        public class DevicemgtuploadbulkvendorOutput : ErrorMessageModel
        {
            
        }
    }
}