﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemegtgetsalescustomerbreakdownModel
    {
        public class DevicemegtgetsalescustomerbreakdownInput
        {
            public int search_type { get; set; }
            public string search_operator { get; set; }
            public Double? search_value { get; set; }
        }
        public class DevicemegtgetsalescustomerbreakdownOutput:ErrorMessageModel
        {
            public int? customer_id { get; set; }
            public string cust_address { get; set; }
            public string Vendor_name { get; set; }
            public int? Order_placed { get; set; }
            public int? Success_order { get; set; }
            public double? Net_sale { get; set; }
            public double? Gross_Profit { get; set; }
            public double? Shipment_cost { get; set; }
            public double? tax { get; set; }
            public double? discount { get; set; }
            public double? Goods_cost { get; set; }
            public string buying_currency { get; set; }
            public string Selling_currency { get; set; }
            public double? exchange_rate { get; set; }
        }
    }
}