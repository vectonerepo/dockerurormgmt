﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemegtgetproductsellingperformanceModel
    {
        public class DevicemegtgetproductsellingperformanceInput
        {
            public int search_type { get; set; }
            public string search_operator { get; set; }
            public Double? search_value { get; set; }
        }
        public class DevicemegtgetproductsellingperformanceOutput:ErrorMessageModel
        {
            public int? Product_id { get; set; }
            public string product_name { get; set; }
            public string Porduct_Type { get; set; }
            public int? Sales_qty { get; set; }
            public double? Sales_order_value { get; set; }
            public double? Sales_Net_Order_value { get; set; }
            public string buying_currency { get; set; }
            public string Selling_currency { get; set; }
            public double? exchange_rate { get; set; }
        }
    }
}