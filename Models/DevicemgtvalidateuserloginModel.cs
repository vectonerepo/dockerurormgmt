﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtvalidateuserloginModel
    {
        public class DevicemgtvalidateuserloginInput
        {
            public string username { get; set; }
            public string password { get; set; }
            public string loginip { get; set; }
        }
        public class DevicemgtvalidateuserloginOutput:ErrorMessageModel
        {
            public int? roleid { get; set; }
            public int? userid { get; set; }           
        }
    }
}