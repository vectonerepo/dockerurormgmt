﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetvendorModel
    {
        public class DevicemgtgetvendorInput
        {
            public int vendor_id { get; set; }
            public int status { get; set; }
        }
        public class DevicemgtgetvendorOutput:ErrorMessageModel
        {
            public int? Vendor_ID { get; set; }
            public string Vendor_name { get; set; }
            public int? status { get; set; }
            public string email_id { get; set; }
            public string phoneno { get; set; }
            public string location { get; set; }
            public DateTime? last_update { get; set; }
            public string status_desc { get; set; }
        }
    }
}