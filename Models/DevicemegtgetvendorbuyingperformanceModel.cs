﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemegtgetvendorbuyingperformanceModel
    {
        public class DevicemegtgetvendorbuyingperformanceInput
        {
            public int search_type { get; set; }
            public string search_operator { get; set; }
            public Double? search_value { get; set; }
        }
        public class DevicemegtgetvendorbuyingperformanceOutput:ErrorMessageModel
        {
            public string Vendor_name { get; set; }
            public string Product_name { get; set; }
            public string brand_name { get; set; }
            public string name { get; set; }
            public double? buying_price { get; set; }
            public string buying_currency { get; set; }
            public int? qty { get; set; }          
            public string Selling_currency { get; set; }
            public double? exchange_rate { get; set; }
        }
    }
}