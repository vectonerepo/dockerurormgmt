﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtreturndevicecheckmacserialnumberModel
    {
        public class DevicemgtreturndevicecheckmacserialnumberInput
        {
            public int return_id { get; set; }
            public string sku_code { get; set; }
            public string mac_address { get; set; }
            public string serial_no { get; set; }
        }
        public class DevicemgtreturndevicecheckmacserialnumberOutput:ErrorMessageModel
        {          
        }
    }
}