﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtvalidatemacserialnoModel
    {
        public class DevicemgtvalidatemacserialnoInput
        {
            public string mac_address { get; set; }
            public string serial_no { get; set; }
        }
        public class DevicemgtvalidatemacserialnoOutput:ErrorMessageModel
        {
        }
    }
}