﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtproductpromotionModel
    {
        public class DevicemgtproductpromotionInput
        {
            public int promoid { get; set; }
            public int status { get; set; }
        }
        public class DevicemgtproductpromotionOutput:ErrorMessageModel
        {
            public int? Promoid { get; set; }
            public int? product_id { get; set; }
            public string Product_name { get; set; }
            public string sku_code { get; set; }
            public string product_Type_name { get; set; }
            public int? product_Type_id { get; set; }
            public DateTime? Start_date { get; set; }
            public DateTime? End_date { get; set; }
            public string Promo_code { get; set; }
            public int? auto_apply { get; set; }
            public int? status { get; set; }
            public DateTime? create_date { get; set; }
            public string create_by { get; set; }
            public DateTime? last_update { get; set; }
            public string last_update_by { get; set; }
            public string status_desc { get; set; }
        }
    }
}