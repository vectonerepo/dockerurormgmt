﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetproductinfoModel
    {
        public class DevicemgtgetproductinfoInput
        {
            public int product_id { get; set; }
            public int vendor_id { get; set; }
            public int brand_id { get; set; }
            public int stock_qty_filter { get; set; }
            public string status { get; set; }
        }
        public class DevicemgtgetproductinfoOutput:ErrorMessageModel
        {
            public int? Product_Id { get; set; }
            public string Product_name { get; set; }
            public string product_desc { get; set; }
            public string sku_code { get; set; }
            public int? Fk_product_type_id { get; set; }
            public int? Fk_brand_Id { get; set; }
            public string buying_currency { get; set; }
            public double? buying_price { get; set; }
            public string selling_currency { get; set; }
            public double? selling_price { get; set; }
            public double? weight { get; set; }
            public int? stock { get; set; }
            public int? status { get; set; }
            public int? Fk_Vendor_ID { get; set; }
            public string img_path1 { get; set; }
            public string img_path2 { get; set; }
            public string img_path3 { get; set; }
            public string img_path4 { get; set; }
            public string img_path5 { get; set; }
            public string product_type_desc { get; set; }
            public string brand_desc { get; set; }
            public string Vendor_name { get; set; }
            public string status_description { get; set; }
            public DateTime? create_date { get; set; }
            public DateTime? last_update { get; set; }
        }
    }
}