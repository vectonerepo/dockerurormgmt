﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtcreatebrandModel
    {
        public class DevicemgtcreatebrandInput
        {
            public string brand_name { get; set; }
            public int status { get; set; }
            public int brandid { get; set; }
            public string create_by { get; set; }
            public int processtype { get; set; }
        }
        public class DevicemgtcreatebrandOutput:ErrorMessageModel
        {
           
        }
    }
}