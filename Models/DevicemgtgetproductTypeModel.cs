﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetproductTypeModel
    {
        
        public class DevicemgtgetproductTypeOutput : ErrorMessageModel
        {
            public int product_Type { get; set; }
            public string name { get; set; }
        }
    }
}