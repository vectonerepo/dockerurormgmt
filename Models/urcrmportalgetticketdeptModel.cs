﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URORMGTDocker.Models
{
    public class urcrmportalgetticketdeptModel
    {
        public class urcrmportalgetticketdeptOutput : ErrorMessageModel
        {
            public int? dept_id { get; set; }
            public string dept_name { get; set; }
        }
    }
}