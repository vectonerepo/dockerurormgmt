﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtupdatereturndeviceinfoModel
    {
        public class DevicemgtupdatereturndeviceinfoInput
        {
            public int return_id { get; set; }
            public string sku_code { get; set; }
            public string comments { get; set; }
            public int return_status { get; set; }
            public string return_reason { get; set; }
            public int Type { get; set; }
            public int return_reason_Type { get; set; }
        }
        public class DevicemgtupdatereturndeviceinfoOutput:ErrorMessageModel
        {
        }
    }
}