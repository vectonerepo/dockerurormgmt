﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetreplacementstatusModel
    {
        public class DevicemgtgetreplacementstatusInput
        {
        }
        public class DevicemgtgetreplacementstatusOutput:ErrorMessageModel
        {
            public int status_id { get; set; }
            public string status_desc { get; set; }          
        }
    }
}