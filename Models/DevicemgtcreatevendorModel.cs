﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtcreatevendorModel
    {
        public class DevicemgtcreatevendorInput
        {
            public string Vendor_name { get; set; }
            public int status { get; set; }
            public string email_id { get; set; }
            public string phoneno { get; set; }
            public string location { get; set; }
            public string create_by { get; set; }
            public int vendor_id { get; set; }
            public int processtype { get; set; }
        }
        public class DevicemgtcreatevendorOutput:ErrorMessageModel
        {
           
        }
    }
}