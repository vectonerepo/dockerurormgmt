﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetproductstatusModel
    {
        public class DevicemgtgetproductstatusInput
        {
        }
        public class DevicemgtgetproductstatusOutput:ErrorMessageModel
        {
            public int status_id { get; set; }
            public string status_desc { get; set; }            
        }
    }
}