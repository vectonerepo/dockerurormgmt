﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetpurchasedordeinfoModel
    {
        public class DevicemgtgetpurchasedordeinfoInput
        {
            public int Search_type { get; set; }
        }
        public class DevicemgtgetpurchasedordeinfoOutput:ErrorMessageModel
        {
            public string order_no { get; set; }
            public string company_name { get; set; }
            public string contact_phone { get; set; }
            public string email { get; set; }
            public string shipping_address { get; set; }
            public string product_name { get; set; }
            public string sku_code { get; set; }
            public string phone_model { get; set; }
            public double? tot_phone_charge { get; set; }
            public int? quantity { get; set; }
            public DateTime? createdate { get; set; }
            public DateTime? shipment_date { get; set; }
            public DateTime? dispatch_date { get; set; }
            public string dev_Status { get; set; }
            public DateTime? delivery_date { get; set; }
            public int? customer_id { get; set; }
        }
    }
}