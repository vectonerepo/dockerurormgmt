﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetreplaceinitiatedordersModel
    {
        public class DevicemgtgetreplaceinitiatedordersInput
        {
            public int status { get; set; }
            public int replace_id { get; set; }
        }
        public class DevicemgtgetreplaceinitiatedordersOutput:ErrorMessageModel
        {
            public int? replace_id { get; set; }
            public int? order_id { get; set; }
            public string prod_code { get; set; }
            public string prod_name { get; set; }
            public string prod_model { get; set; }
            public DateTime? rpl_date { get; set; }
            public int? rpl_quantity { get; set; }
            public string rpl_reason { get; set; }
            public int? rpl_status { get; set; }
            public DateTime? replace_date { get; set; }
            public DateTime? last_update { get; set; }
            public DateTime? delivery_date { get; set; }
            public DateTime? shipping_date { get; set; }
            public string shipping_address { get; set; }
            public string email_id { get; set; }
            public string Mobileno { get; set; }
            public int? customer_id { get; set; }
            public string rpl_status_desc { get; set; }
            public string sku_code { get; set; }
        }
    }
}