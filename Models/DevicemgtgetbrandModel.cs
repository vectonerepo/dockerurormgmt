﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetbrandModel
    {
        public class DevicemgtgetbrandInput
        {
            public int brand_id { get; set; }
        }
        public class DevicemgtgetbrandOutput:ErrorMessageModel
        {
            public int brand_id { get; set; }
            public string brand_name { get; set; }
            public int status { get; set; }           
        }
    }
}