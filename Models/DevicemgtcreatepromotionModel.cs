﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtcreatepromotionModel
    {
        public class DevicemgtcreatepromotionInput
        {
            public int? product_id { get; set; }
            public float? Min_pur_discount { get; set; }
            public int? product_Type { get; set; }
            public DateTime? Start_date { get; set; }
            public DateTime? end_date { get; set; }
            public string Promo_code { get; set; }
            public int? auto_apply { get; set; }
            public int? status { get; set; }
            public string calledby { get; set; }
            public int? promoid { get; set; }
            public int? process_Type { get; set; }
        }
        public class DevicemgtcreatepromotionOutput:ErrorMessageModel
        {
           
        }
    }
}