﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class ErrorMessageModel
    {
        public string Message { get; set; }
        public int? Code { get; set; }
    }
}