﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetreturnstatusModel
    {
        public class DevicemgtgetreturnstatusInput
        {
        }
        public class DevicemgtgetreturnstatusOutput:ErrorMessageModel
        {
            public int status_id { get; set; }
            public string status_desc { get; set; }
        }
    }
}