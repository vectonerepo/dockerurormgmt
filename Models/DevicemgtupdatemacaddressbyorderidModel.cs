﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtupdatemacaddressbyorderidModel
    {
        public class DevicemgtupdatemacaddressbyorderidInput
        {
            public int order_id { get; set; }
            public string mac_address { get; set; }
            public string serial_number { get; set; }
        }
        public class DevicemgtupdatemacaddressbyorderidOutput:ErrorMessageModel
        {
        }
    }
}