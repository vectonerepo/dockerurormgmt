﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtupdatedevicereturnstatusModel
    {
        public class DevicemgtupdatedevicereturnstatusInput
        {
            public int returnid { get; set; }
            public int status { get; set; }
        }
        public class DevicemgtupdatedevicereturnstatusOutput:ErrorMessageModel           
        {         
        }
    }
}