﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtupdatedispatchreplaceinfoModel
    {
        public class DevicemgtupdatedispatchreplaceinfoInput
        {
            public int replace_id { get; set; }
            public string mac_address { get; set; }
            public string serial_number { get; set; }
            public int status { get; set; }
            public int Type { get; set; }
            public DateTime? shipping_date { get; set; }
            public string track_number { get; set; }
            public string tracking_url { get; set; }
        }
        public class DevicemgtupdatedispatchreplaceinfoOutput:ErrorMessageModel
        {
            
        }
    }
}