﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetpurchaeddevicebyorderidModel
    {
        public class DevicemgtgetpurchaeddevicebyorderidInput
        {
            public int order_id { get; set; }
        }
        public class DevicemgtgetpurchaeddevicebyorderidOutput:ErrorMessageModel
        {
            public string order_no { get; set; }
            public string Product_name { get; set; }
            public string sku_code { get; set; }
            public int customer_id { get; set; }
            public string email { get; set; }
            public string mobileno { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string postcode { get; set; }
            public string country { get; set; }
            public string dev_Status { get; set; }
        }
    }
}