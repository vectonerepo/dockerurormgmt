﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetreplacementorderinfoModel
    {
        public class DevicemgtgetreplacementorderinfoInput
        {
            public int status { get; set; }
            public int replace_id { get; set; }
        }
        public class DevicemgtgetreplacementorderinfoOutput:ErrorMessageModel
        {
            public int? id { get; set; }
            public int? order_id { get; set; }
            public string prod_code { get; set; }
            public string prod_name { get; set; }
            public string phone_model { get; set; }
            public string prod_model { get; set; }
            public DateTime? rpl_date { get; set; }
            public int? rpl_quantity { get; set; }
            public string rpl_reason { get; set; }
            public int? rpl_status { get; set; }
            public string tracking_no { get; set; }
            public string comments { get; set; }
            public DateTime? delivery_date { get; set; }
            public string company_name { get; set; }
            public string delivery_comment { get; set; }
            public DateTime? order_date { get; set; }
            public string referenceid { get; set; }
            public DateTime? last_update { get; set; }
            public string rpl_status_desc { get; set; }
            public string product_sku { get; set; }
            public DateTime? shipping_date { get; set; }
            public string shipping_address { get; set; }
            public string email_id { get; set; }
            public string Mobileno { get; set; }
            public int? customer_id { get; set; }

//            shipping_address string
//email_id    string
//Mobileno    string
//customer_id integer
        }
    }
}