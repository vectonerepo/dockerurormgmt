﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtupdatestatusbyorderidModel
    {
        public class DevicemgtupdatestatusbyorderidInput
        {
            public int? order_id { get; set; }
            public int? status { get; set; }
            public string tracking_url { get; set; }
            public string tracking_no { get; set; }
            public DateTime? shipment_date { get; set; }
        }
        public class DevicemgtupdatestatusbyorderidOutput:ErrorMessageModel
        {
           
        }
    }
}