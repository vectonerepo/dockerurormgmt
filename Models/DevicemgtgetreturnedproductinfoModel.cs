﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace URORMGTDocker.Models
{
    public class DevicemgtgetreturnedproductinfoModel
    {
        public class DevicemgtgetreturnedproductinfoInput
        {
            public int Search_type { get; set; }
            public int retrun_id { get; set; }
        }
        public class DevicemgtgetreturnedproductinfoOutput:ErrorMessageModel
        {
            public string order_no { get; set; }
            public string company_name { get; set; }
            public string contact_phone { get; set; }
            public string email { get; set; }
            public string shipping_address { get; set; }
            public string product_name { get; set; }
            public string sku_code { get; set; }
            public string phone_model { get; set; }
            public double? rtn_charge { get; set; }
            public int? quantity { get; set; }
            public DateTime? return_Date { get; set; }
            public DateTime? shipment_date { get; set; }
            public DateTime? dispatch_date { get; set; }
            public string rtn_reason { get; set; }
            public int? status_id { get; set; }
            public string status_desc { get; set; }
            public int? return_id { get; set; }
            public string payment_mode { get; set; }
            public int? customer_id { get; set; }
        }
    }
}