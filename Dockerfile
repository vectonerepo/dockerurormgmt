FROM mcr.microsoft.com/dotnet/sdk:5.0
#FROM microsoft/dotnet:latest
#Proxy
ENV http_proxy "http://10.30.2.24:8080"
ENV https_proxy "http://10.30.2.24:8080"

#RUN apt update -y

#RUN cd ; wget https://www.openssl.org/source/openssl-1.1.1.tar.gz ; tar -zxf openssl-1.1.1.tar.gz && cd openssl-1.1.1

#RUN cd /root/openssl-1.1.1 ; ./config enable-ssl2 enable-ssl3

#RUN apt -y install make gcc

#RUN cd /root/openssl-1.1.1 ; make

#RUN cd /root/openssl-1.1.1 ; make test

#RUN mv /usr/bin/openssl ~/tmp

#RUN cd /root/openssl-1.1.1 ; make install

#RUN ln -s /usr/local/bin/openssl /usr/bin/openssl

#RUN openssl version

COPY . /app

WORKDIR /app

RUN sed -i 's/^CipherString.*/CipherString = DEFAULT@SECLEVEL=1/g' /etc/ssl/openssl.cnf

RUN sed -i 's/^MinProtocol.*/MinProtocol = TLSv1/g' /etc/ssl/openssl.cnf

RUN ["dotnet", "restore"]
RUN ["dotnet", "build"]

EXPOSE 5000/tcp
ENV ASPNETCORE_URLS http://*:5000

ENTRYPOINT ["dotnet", "run"]

